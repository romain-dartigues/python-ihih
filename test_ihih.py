# vim:set fileencoding=utf8:

import os
import sys
import tempfile
import unittest


import ihih





noop = lambda _: _
PY2 = sys.version_info[0] == 2
PY3 = sys.version_info[0] == 3



encode = noop
IOError_compat = OSError

if PY2:
    encode = lambda _,e='utf8':_.encode(e)
    IOError_compat = IOError



class SimpleConf(unittest.TestCase):
    _key = 0

    def __init__(self, *args, **kwargs):
        super(SimpleConf, self).__init__(*args, **kwargs)
        if os.geteuid() == 0:
            # do not run tests as root please
            os.seteuid(1000)


    def setUp(self):
        self.ihih = ihih.IHIH(())

        tmp_b = tempfile.NamedTemporaryFile()
        tmp_b.write(b'  debug  =  0 \nfoo = bar # comment\n\n#var = c')
        tmp_b.flush()

    def key(self, new=False):
        if new:
            self._key+= 1
        return self._key


    def test_value_default(self):
        conf = self.ihih.__class__((), default=1)
        self.assertTrue('default' in conf)
        self.assertEqual(conf, {'default': 1})

    def test_value_simple(self):
        self.ihih[self.key(True)] = 'simple value'
        self.assertEqual(self.ihih[self.key()], 'simple value')

    def test_quoting_simple(self):
        self.ihih[self.key(True)] = '''simple 'quoted' "value"'''
        self.assertEqual(self.ihih[self.key()], 'simple quoted value')

    def test_quoting_interleaved(self):
        self.ihih[self.key(True)] = '''interleaved: 'a"'"'b"'''
        self.assertEqual(self.ihih[self.key()], '''interleaved: a"'b''')

    def test_quoting_escaped(self):
        self.ihih[self.key(True)] = r'''"\"''"'''
        self.assertEqual(self.ihih[self.key()], '"\'\'')

    def test_escaping(self):
        self.ihih[self.key(True)] = r'\r\n\0\"\$'
        self.assertEqual(self.ihih[self.key()], r'\r\n\0"\$')


    def test_file_loading_no_file(self):
        self.assertEqual(ihih.IHIH(()), {})

    def test_file_loading_one_file(self):
        with tempfile.NamedTemporaryFile() as tmp:
            self.assertEqual(ihih.IHIH(tmp.name), {})
            self.assertEqual(ihih.IHIH([tmp.name]), {})
            self.assertEqual(ihih.IHIH((tmp.name,)), {})


    def test_file_loading_many_files(self):
        with tempfile.NamedTemporaryFile() as tmp_a,\
        tempfile.NamedTemporaryFile() as tmp_b:
            self.assertEqual(ihih.IHIH((tmp_a.name, tmp_b.name)), {})
            self.assertEqual(ihih.IHIH([tmp_a.name, tmp_b.name]), {})


    def test_parse_openning_error(self):
        with tempfile.NamedTemporaryFile() as tmp:
            os.chmod(tmp.name, 0o000)
            self.assertFalse(os.geteuid() == 0, 'this test cannot be ran as root')
            with self.assertRaises(IOError_compat):
                self.ihih.parse(tmp.name, ignore_errors=False)

            self.assertFalse(self.ihih.parse(tmp.name, ignore_errors=True))

        # check the os.stat / file not found
        self.assertFalse(self.ihih.parse(tmp.name, ignore_errors=True))


    def test_parse_openning_permission_denied(self):
        tmp = tempfile.mkdtemp()
        os.chmod(tmp, 0o444)
        with self.assertRaises(OSError):
            self.ihih.parse(os.path.join(tmp, 'file'), ignore_errors=False)
        os.rmdir(tmp)


    def test_file_loading(self):
        tmp_a = tempfile.NamedTemporaryFile()
        tmp_a.write(b'debug = 1\nvar = x y')
        tmp_a.flush()

        tmp_b = tempfile.NamedTemporaryFile()
        tmp_b.write(b'  debug  =  0 \nfoo = bar # comment\n\n#var = c')
        tmp_b.flush()

        conf = self.ihih.__class__((tmp_a.name, tmp_b.name))
        self.assertEqual(conf['debug'], '0')
        self.assertEqual(conf['var'],   'x y')
        self.assertEqual(conf['foo'],   'bar')

        tmp_a.close()
        tmp_b.close()

    def test_file_loading_unicode(self):
        tmp = tempfile.NamedTemporaryFile()
        tmp.write(u'größe = 10.24\nwährung = €'.encode('utf8'))
        tmp.flush()

        conf = self.ihih.__class__(tmp.name)
        self.assertEqual(conf[u'größe'], '10.24')
        self.assertEqual(conf[u'währung'], u'€')
        self.assertEqual(conf.get_text(u'währung'), u'€')

        tmp.close()


    def test_file_reloading_unneeded(self):
        with tempfile.NamedTemporaryFile() as tmp:
            conf = ihih.IHIH(tmp.name)
            self.assertIsNone(conf.parse(tmp.name))


    def test_type_conversion(self):
        f = 1.1
        self.ihih[f] = f
        self.assertEqual(self.ihih[f], str(f))
        self.assertEqual(self.ihih[str(f)], str(f))
        self.assertEqual(self.ihih.get(f), str(f))
        self.assertEqual(self.ihih.get_float(f), f)

    def test_comments(self):
        self.ihih[self.key(True)] = 'value # with comment'
        self.assertEqual(self.ihih[self.key()], 'value')

        self.ihih[self.key(True)] = 'value // with comment'
        self.assertEqual(self.ihih[self.key()], 'value')

        self.ihih[self.key(True)] = ''
        self.assertEqual(self.ihih.get_bool(self.key()), None)

    def test_comments_escaped(self):
        self.ihih[self.key(True)] = r'value with \# # and a comment'
        self.assertEqual(self.ihih[self.key()], 'value with #')

        self.ihih[self.key(True)] = r'value with \// /\/ \/\/ // and a comment'
        self.assertEqual(self.ihih[self.key()], 'value with // // //')

    def test_quoting_comments(self):
        example_url = 'http://example.com/'

        self.ihih[self.key(True)] = example_url
        self.assertEqual(self.ihih[self.key()], 'http:')

        self.ihih[self.key(True)] = '"' + example_url + '"'
        self.assertEqual(self.ihih[self.key()], example_url)

        self.ihih[self.key(True)] = "'" + example_url + "'"
        self.assertEqual(self.ihih[self.key()], example_url)

    def test_comments_quotes(self):
        self.ihih[self.key(True)] = 'a # b "c"'
        self.assertEqual(self.ihih[self.key()], 'a')

        self.ihih[self.key(True)] = 'a#b "c"'
        self.assertEqual(self.ihih[self.key()], 'a')

        self.ihih[self.key(True)] = '-0'
        self.assertEqual(self.ihih.get_float(self.key()), -0.0)

    def test_get_bool(self):
        self.ihih[self.key(True)] = 'yes # comment'
        self.assertEqual(self.ihih.get_bool(self.key()), True)

        self.ihih[self.key(True)] = 'False # comment'
        self.assertEqual(self.ihih.get_bool(self.key()), False)

        self.ihih[self.key(True)] = 'unknown'
        self.assertEqual(self.ihih.get_bool(self.key(), 'na'), 'na')

        self.ihih[self.key(True)] = ''
        self.assertEqual(self.ihih.get_bool(self.key()), None)

        self.ihih[self.key(True)] = ' 42 '
        self.assertEqual(self.ihih.get_int(self.key()), 42)


    def test_get_default(self):
        key = self.key(True)
        self.assertNotIn(key, self.ihih)
        self.assertIsNone(self.ihih.get(key))
        self.assertEqual(self.ihih.get(key, 'foo'), 'foo')


    def test_cast_as_text(self):
        # check the key and the values are of textual type
        self.ihih[None] = None
        self.assertEqual(self.ihih['None'], 'None')

        self.ihih[0.0] = 0.0
        self.assertEqual(self.ihih['0.0'], '0.0')


    def test_get_float(self):
        self.ihih[self.key(True)] = '3.14'
        self.assertEqual(self.ihih.get_float(self.key()), 3.14)

        self.ihih[self.key(True)] = '314e-2'
        self.assertEqual(self.ihih.get_float(self.key()), 3.14)

        self.ihih[self.key(True)] = '-0'
        self.assertEqual(self.ihih.get_float(self.key()), -0.0)

        self.ihih[self.key(True)] = ' 052 '
        self.assertEqual(self.ihih.get_int(self.key(), base=8), 42)

        self.ihih[self.key(True)] = 'invalid'
        self.assertRaises(ValueError, self.ihih.get_float, self.key())
        self.assertIsNone(self.ihih.get_float(self.key(), errors='ignore'))

        self.assertIsNone(self.ihih.get_float(self.key(True)))


    def test_get_int(self):
        self.ihih[self.key(True)] = '42'
        self.assertEqual(self.ihih.get_int(self.key()), 42)

        self.ihih[self.key(True)] = ' 42 '
        self.assertEqual(self.ihih.get_int(self.key()), 42)

        self.ihih[self.key(True)] = '2a'
        self.assertEqual(self.ihih.get_int(self.key(), base=16), 42)

        self.ihih[self.key(True)] = ' 0x2a '
        self.assertEqual(self.ihih.get_int(self.key(), base=16), 42)

        self.ihih[self.key(True)] = '52'
        self.assertEqual(self.ihih.get_int(self.key(), base=8), 42)

        self.ihih[self.key(True)] = ' 052 '
        self.assertEqual(self.ihih.get_int(self.key(), base=8), 42)

        self.ihih[self.key(True)] = '42.0'
        self.assertRaises(ValueError, self.ihih.get_int, self.key())

        self.assertIsNone(self.ihih.get_int(self.key(True)))


    def test_del(self):
        val = '42'
        self.ihih[self.key(True)] = val
        self.assertEqual(self.ihih[self.key()], val)
        del self.ihih[self.key()]
        self.assertNotIn(self.key(), self.ihih)



class Interpolated(SimpleConf):
    def setUp(self):
        self.ihih = ihih.IHIHI(())


    def test_escaping(self):
        self.ihih[self.key(True)] = r'\r\n\0\"\$'
        self.assertEqual(self.ihih[self.key()], r'\r\n\0"$')


    def test_variable_interpolation(self):
        self.ihih['a'] = r'simple \"value\"'
        self.assertEqual(self.ihih['a'], r'simple "value"')

        self.ihih['b'] = r'\$a=$a=$a'
        self.assertEqual(self.ihih['b'], r'$a=%(a)s=%(a)s' % self.ihih, 'simple interpolation')

        self.ihih['c'] = r'$d $a'
        self.assertEqual(self.ihih['c'], r' %(a)s' % self.ihih, 'interpolation with unknown value')

        self.ihih['d'] = r'd'
        self.assertEqual(self.ihih['c'], r'd %(a)s' % self.ihih, 'interpolation lazy resolution')


    def test_variable_interpolation_recursion(self):
        self.ihih['a'] = r'a'

        self.ihih['b'] = r'\$b=$b'
        self.assertEqual(self.ihih['b'], r'$b=', 'direct recursion')

        self.ihih['c'] = r'd[$d]'
        self.ihih['d'] = r'c[$c]'
        self.assertEqual(self.ihih['c'], r'd[c[]]', 'indirect recursion')
        self.assertEqual(self.ihih['d'], r'c[d[]]', 'indirect recursion')


    def test_variable_boundaries(self):
        self.ihih['a'] = 'var:a'
        self.ihih['{a}'] = 'var:{a}'
        self.ihih['a b'] = 'var:{a b}'
        self.ihih['a_b'] = 'var:a_b'
        self.ihih['a.b'] = 'var:a.b'
        self.ihih['a:b'] = 'var:a:b'

        self.ihih[self.key(True)] = r'$a=${a}'
        self.assertEqual(self.ihih[self.key()], r'%(a)s=%(a)s' % self.ihih)

        self.ihih[self.key(True)] = r'$a_b=${a_b}'
        self.assertEqual(self.ihih[self.key()], r'%(a_b)s=%(a_b)s' % self.ihih)

        self.ihih[self.key(True)] = r'$a.b=${a.b}'
        self.assertEqual(self.ihih[self.key()], r'%(a.b)s=%(a.b)s' % self.ihih)

        self.ihih[self.key(True)] = r'$a:b=${a:b}'
        self.assertEqual(self.ihih[self.key()], r'%(a:b)s=%(a:b)s' % self.ihih)

        self.ihih[self.key(True)] = r'$a b!=${a b}'
        self.assertEqual(self.ihih[self.key()], r'%(a)s b!=%(a b)s' % self.ihih)

        self.ihih[self.key(True)] = r'${{a}}'
        self.assertEqual(self.ihih[self.key()], r'}')

        self.ihih[self.key(True)] = r'${{a\}}=${\{a\}}'
        self.assertEqual(self.ihih[self.key()], r'%({a})s=%({a})s' % self.ihih)


    def test_variable_unicode(self):
        self.ihih[u'größe'] = 10.24
        self.ihih[u'währung'] = u'€'

        self.ihih[self.key(True)] = u'$größe $währung'

        self.assertEqual(self.ihih.get_text(self.key()), u'10.24 €')


    def test_variable_quoting(self):
        self.ihih['a'] = 'AA'
        self.ihih['b'] = ''' "[$a]" '[$a]' '''

        self.assertEqual(self.ihih['b'], ' [AA] [$a] ')





if __name__ == '__main__':
    unittest.main()
